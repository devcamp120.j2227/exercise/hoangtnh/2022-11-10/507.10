// Import thư viện Express Js
const express = require("express");

// Khởi tạo 1 app express
const app = express();

// Khai báo 1 cổng 
const port = 8000;

// Khai báo để lấy được request body json
app.use(express.json());

let array = ["Xanh", "Đỏ", "Tím", "Vàng"];

// Khai báo GET API trả ra simple message
app.get("/", (request, response) => {
    let today = new Date();

    let string = `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`;

    response.json({
        message: string
    })
})

// Khai báo phương thức GET
app.get("/get-method", (request, response) => {
    response.json({
        array: array
    })
})

// Khai báo phương thức POST
app.post("/post-method", (request, response) => {
    response.json({
        array: [...array, "Hồng"]
    })
})

// Khai báo phương thức PUT
app.put("/put-method", (request, response) => {
    array[3] = "Nâu";

    response.json({
        array: array
    })
})

// Khai báo phương thức DELETE
app.delete("/delete-method", (request, response) => {
    let [first, ...slice_array] = array;

    response.json({
        array: slice_array
    })
})

// Khai báo API sử dụng request param
app.get("/request-params/:param", (request, response) => {
    const request_param = request.params.param;

    response.json({
        result: array[request_param]
    })
})

// Khai báo API sử dụng request query
app.get("/request-query", (request, response) => {
    const query = request.query;

    const color = query.color;

    const find_color = array.findIndex((element) => element == color);

    response.json({
        result: find_color
    })
})

// Khai báo API sử dụng body json
app.post("/request-body-json", (request, response) => {
    const body = request.body;

    response.json({
        result: body
    })
})

// Chạy app trên cổng đã khai báo
app.listen(port, () => {
    console.log(`App đã chạy trên cổng ${port}`)
})